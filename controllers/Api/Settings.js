'use strict';
const Setting = require('../../models/Setting');
const Helper = require('../../middleware/Helper');

const inside = require('point-in-polygon')

const success_status = process.env.SUCCESS_STATUS;
const error_status = process.env.ERROR_STATUS;

class Settings {

  function(){
  }

  async create(req, res, next) {
    const newSetting = new Setting(req.body)
    newSetting.save(function(err, setting) {
      if (err) {
        return res.send({
          "statusCode": error_status,
          "msg": err
        });
      }else {
        return res.send({
          "setting": setting,
          "statusCode": success_status
        });
      }
    });
  }

  async update(req, res, next) {
    Setting.updateOne({
        _id: req.body._id
      }, {
        $set: req.body
      },
      function(err) {
        if (err) {
          return res.send({
            "statusCode": error_status
          });
        }else {
          return res.send({
            "statusCode": success_status
          });
        }
      });
   }

   async search(req, res, next) {
     Setting.find(req.body).then(function(settings){
       if (settings) {
         return res.send({
           "settings": settings,
           "statusCode": success_status
         });
       } else {
         return res.send({
           "statusCode": error_status
         });
       }
     })
   }

   async checkAvailability(req, res, next) {
     Setting.findOne({type:"area"}).then(function(area){
       if (area) {
           const p1 = area.coordinates.p1;
           const p2 = area.coordinates.p2;
           const p3 = area.coordinates.p3;
           const p4 = area.coordinates.p4;

           var polygon = [
            [p1.latitude, p1.longitude],
            [p2.latitude, p2.longitude],
            [p3.latitude, p3.longitude],
            [p4.latitude, p4.longitude]
            ];
          var isInside = inside([req.body.latitude, req.body.longitude], polygon)
          if (isInside)
            return res.send({
              "statusCode": success_status
            });
          return res.send({
            "statusCode": error_status
          });
       } else {
         return res.send({
           "statusCode": error_status
         });
       }
     })
   }

   async createCheckoutID(req, res, next) {
       const amount = req.body.amount;
       if (amount == undefined || amount == "" || isNaN(amount)) {
         return res.send({
           statusCode: error_status
         });
       }else {
         console.log("Amount - " + amount);
         Helper.createCheckoutID(amount).then(function(response) {
             console.log("Payment ID - " + JSON.stringify(response));
             return res.send({
               record: response.id,
               statusCode: success_status
             });
           }
         ).catch(error => {
           console.log("Error - " + error);
           return res.send({
             statusCode: error_status
           });
         })
       }
   }

   async checkPaymentStatus(req, res, next) {
     const checkoutID = req.body.checkoutID;
     Helper.checkPaymentStatus(checkoutID).then(function(response) {
         console.log("Payment Status - " + JSON.stringify(response));
         return res.send({
           statusCode: success_status,
           record: response
         });
       }
     ).catch(error => {
       console.log("Error - " + error);
       return res.send({
         statusCode: error_status
       });
     })
   }
}

module.exports = Settings;
