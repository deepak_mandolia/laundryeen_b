'use strict';
const Cloth = require('../../models/Cloth');

const success_status = process.env.SUCCESS_STATUS;
const error_status = process.env.ERROR_STATUS;

class Clothes {

  function(){
  }

  async create(req, res, next) {
    const newCloth = new Cloth(req.body)
    newCloth.save(function(err, cloth) {
      if (err) {
        return res.send({
          "statusCode": error_status,
          "msg": err
        });
      }else {
        return res.send({
          "record": cloth,
          "statusCode": success_status
        });
      }
    });
  }

  async update(req, res, next) {
    Cloth.updateOne({
        _id: req.body._id
      }, {
        $set: req.body
      },
      function(err) {
        if (err) {
          return res.send({
            "statusCode": error_status
          });
        }else {
          return res.send({
            "statusCode": success_status
          });
        }
      });
   }

   async search(req, res, next) {
     Cloth.find(req.body).then(function(clothes){
       if (clothes) {
         return res.send({
           "record": clothes,
           "statusCode": success_status
         });
       } else {
         return res.send({
           "statusCode": error_status
         });
       }
     })
   }
}

module.exports = Clothes;
