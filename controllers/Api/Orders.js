'use strict';
const Order = require('../../models/Order');
const Helper = require('../../middleware/Helper');
const Setting = require('../../models/Setting');
const Customer = require('../../models/Customer');

const success_status = process.env.SUCCESS_STATUS;
const error_status = process.env.ERROR_STATUS;

class Orders {

  function(){
  }

  async create(req, res, next) {
    const newOrder = new Order(req.body);
    newOrder.status = 0;
    newOrder.paymentStatus = 0;

    newOrder.startTime = await Helper.getCurrentTime();

    const minimum = await Setting.findOne({type:"minimumOrder"});
    newOrder.minimum = minimum.value;

    const customer = await Customer.findOne({_id:req.body.customer});
    console.log("Bag Fee - " + customer.bagNo);
    if (customer.bagNo == "") {
      const bagFee = await Setting.findOne({type:"bagFee"});
      console.log("Customer has no bag - " + bagFee.value);
      newOrder.bagFee = bagFee.value;
    }

    newOrder.save(function(err, order) {
      if (err) {
        return res.send({
          "statusCode": error_status,
          "msg": err
        });
      }else {
        Helper.sendNotificationToAllUsers("Order", "New Order Received");
        return res.send({
          "statusCode": success_status,
          "record": order
        });
      }
    });
  }

  async update(req, res, next) {
    const order = await Order.findOne({_id:req.body._id});

    if (order.total!= undefined && req.body.total != undefined && order.total != req.body.total && order.paymentStatus == 1) {
      return res.send({
        "statusCode": error_status,
        "msg": "Customer already Paid"
      });
    } else {
      if (req.body.status == 3 && order.invoiceTime == undefined) {
        req.body.invoiceTime = await Helper.getCurrentTime();
      }
      else if (req.body.status == 6 && order.endTime == undefined) {
        req.body.endTime = await Helper.getCurrentTime();
      }

      Order.updateOne({
          _id: req.body._id
        }, {
          $set: req.body
        },
        function(err) {
          if (err) {
            return res.send({
              "statusCode": error_status,
              "err": err
            });
          }else {
            Helper.sendNotificationToCustomer(order.customer, "Order", "Order Updated");
            return res.send({
              "statusCode": success_status
            });
          }
        });
    }
   }

   async search(req, res, next) {
     Order.find(req.body)
     .populate("customer")
     .then(function(orders){
       if (orders) {
         return res.send({
           "record": orders,
           "statusCode": success_status
         });
       } else {
         return res.send({
           "statusCode": error_status
         });
       }
     })
   }

   async splashScreen(req, res, next) {
     const customer = req.body.customer;

     var response = {statusCode: success_status}
     const android = await Setting.findOne({type:"androidVersion"});
     const ios = await Setting.findOne({type:"iosVersion"});
     response.android = android.value;
     response.ios = ios.value;

     if (customer != undefined) {
       Order.findOne({
         customer: customer,
         status: {
            $in: [0, 1, 2, 3, 4, 5, 6]
            }
        }).then(function(order) {
         if (order) {
           response.record = order;
         }
         return res.send(response);
       })
     } else {
       return res.send(response);
     }
   }
}

module.exports = Orders;
