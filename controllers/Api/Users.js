'use strict';
const User = require('../../models/User');
const bcrypt = require('bcrypt');

const success_status = process.env.SUCCESS_STATUS;
const error_status = process.env.ERROR_STATUS;
var salt_rounds = process.env.SALT_ROUNDS;

class Users {

  function(){
  }

  async register(req, res, next) {
    console.log("Data - " + JSON.stringify(req.body));

    User.findOne({email:req.body.email}).then(function(user) {
      if (user) {
        console.log("User Found - " + user.email)
        return res.send({
          "statusCode": error_status,
          "msg": "User already exist"
        });
      } else {
        console.log("User does not exist")
        var newUser = new User(req.body);
        const salt = bcrypt.genSaltSync(Number(salt_rounds));
        const hash = bcrypt.hashSync(req.body.password, salt);
        newUser.password = hash;

        newUser.save(function(err, user) {
          if (err)
            return res.send({
              "statusCode": error_status,
              "err": err
            });
          return res.send({
            "statusCode": success_status
          });
        });
      }
    })
  }

  async login(req,res,next){
    User.findOne({email:req.body.email}).then(function(user){
      if (user) {
        const compare = bcrypt.compareSync(req.body.password, user.password);
        if (compare)
          user.firebaseToken = req.body.firebaseToken;
          user.save();
          return res.send({
            "record": user,
            "statusCode": success_status
          });
        return res.send({
          "statusCode": error_status,
        });
      } else {
        return res.send({
          "statusCode": error_status,
        });
      }
    })

  }

}

module.exports = Users;
