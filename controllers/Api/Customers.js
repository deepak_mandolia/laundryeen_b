'use strict';
const Customer = require('../../models/Customer');
const Helper = require('../../middleware/Helper');

const success_status = process.env.SUCCESS_STATUS;
const error_status = process.env.ERROR_STATUS;

class Customers {

  function(){
  }

  async login_register(req, res, next) {
    var name = req.body.name;
    var mobile = req.body.mobile;
    var device = req.body.device;
    var firebaseToken = req.body.firebaseToken;

    if (name != "" && name != undefined && mobile != "" && mobile != undefined) {
      const otp = await Helper.generateOTP();
      const message = "Your One Time Code for Laundryeen: " + otp
      await Helper.sendSMS(message, mobile)

      Customer.findOne({mobile:mobile}).then(function(customer) {
        if (customer) {
          console.log("existing customer");
          customer.name = name;
          customer.otp = otp;
          customer.device = device;
          customer.firebaseToken = firebaseToken;
          customer.save();
          return res.send({
            "record": customer,
            "statusCode": success_status
          });
        } else {
          console.log("new customer");
          const newCustomer = new Customer(req.body);
          newCustomer.otp = otp;
          newCustomer.bagNo = "";
          newCustomer.save(function(err, customer) {
            if (err) {
              return res.send({
                "statusCode": error_status,
                "msg": err
              });
            }else {
              return res.send({
                "record": customer,
                "statusCode": success_status
              });
            }
          });
        }
      })
    } else {
      return res.send({
        "statusCode": error_status
      });
    }
  }

  async verify_otp(req, res, next) {
    console.log("Verify OTP - " + JSON.stringify(req.body))

    Customer.findOne(req.body).then(function(customer) {
      if (customer) {
        return res.send({
          "statusCode": success_status
        });
      } else {
        return res.send({
          "statusCode": error_status
        });
      }
    })
  }


  async update(req, res, next) {
    Customer.updateOne({
        _id: req.body._id
      }, {
        $set: req.body
      },
      function(err) {
        if (err) {
          return res.send({
            "statusCode": error_status
          });
        }else {
          return res.send({
            "statusCode": success_status
          });
        }
      });
   }

  async search(req, res, next) {
    Customer.find(req.body).then(function(customers){
      if (customers) {
        return res.send({
          "record": customers,
          "statusCode": success_status
        });
      } else {
        return res.send({
          "statusCode": error_status
        });
      }
    })
  }
}

module.exports = Customers;
