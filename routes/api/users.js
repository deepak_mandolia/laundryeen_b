const express =  require('express');
const path  = require('path');
const router = express.Router();
const Users = require("../../controllers/Api/Users") ;
var controllersObject = new Users();
router.post('/login',controllersObject.login);
router.post('/register',controllersObject.register);

module.exports = router;
