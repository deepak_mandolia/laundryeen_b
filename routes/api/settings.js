const express =  require('express');
const path  = require('path');
const router = express.Router();
const Settings = require("../../controllers/Api/Settings") ;
var controllersObject = new Settings();
router.post('/create',controllersObject.create);
router.post('/update',controllersObject.update);
router.post('/search',controllersObject.search);
router.post('/checkAvailability',controllersObject.checkAvailability);
router.post('/createCheckoutID',controllersObject.createCheckoutID);
router.post('/checkPaymentStatus',controllersObject.checkPaymentStatus);

module.exports = router;
