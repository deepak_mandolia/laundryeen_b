const express =  require('express');
const path  = require('path');
const router = express.Router();
const Customers = require("../../controllers/Api/Customers") ;
var controllersObject = new Customers();
router.post('/login_register',controllersObject.login_register);
router.post('/verify_otp',controllersObject.verify_otp);
router.post('/search',controllersObject.search);
router.post('/update',controllersObject.update);

module.exports = router;
