const express =  require('express');
const path  = require('path');
const router = express.Router();
const Clothes = require("../../controllers/Api/Clothes") ;
var controllersObject = new Clothes();
router.post('/create',controllersObject.create);
router.post('/update',controllersObject.update);
router.post('/search',controllersObject.search);

module.exports = router;
