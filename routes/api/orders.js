const express =  require('express');
const path  = require('path');
const router = express.Router();
const Orders = require("../../controllers/Api/Orders") ;
var controllersObject = new Orders();
router.post('/create',controllersObject.create);
router.post('/update',controllersObject.update);
router.post('/search',controllersObject.search);
router.post('/splashScreen',controllersObject.splashScreen);

module.exports = router;
