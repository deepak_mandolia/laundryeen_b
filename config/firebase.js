var firebase = require("firebase-admin");

var serviceAccount = require("./firebasekey.json");


firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
})

module.exports = firebase;
