const express = require("express");
const path = require('path');
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const dotEnv = require('dotenv').config();
const routes = require('./routes');

var port = process.env.PORT;
var dbURI = process.env.DB_URI;
var isProduction = process.env.NODE_ENV === 'production';

const app = express();
app.use(express.static("uploads"));
app.use(express.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(routes);

var dbOptions = {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: true
};

if(isProduction){
  mongoose.connect(dbURI, dbOptions);
} else {
  mongoose.connect("mongodb://ec2-15-184-160-59.me-south-1.compute.amazonaws.com:27017/laundrydb", dbOptions);
}


var server = app.listen( port || 3000, function(){
  console.log('Listening on port ' + server.address().port);
});
