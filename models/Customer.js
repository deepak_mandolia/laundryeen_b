const mongoose = require("mongoose");
const autoIncrement = require('mongoose-sequence')(mongoose);

//Schema
const customerSchema = new mongoose.Schema({
  name: String,
  mobile: String,
  device: Number,
  firebaseToken: String,
  otp: String,
  bagNo: String,
});
customerSchema.plugin(autoIncrement, {
  inc_field: "customerID"
});

const Customer = new mongoose.model("Customer", customerSchema);

module.exports = Customer;
