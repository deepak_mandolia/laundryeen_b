const mongoose = require("mongoose");

//Schema
const clothesSchema = new mongoose.Schema({
  nameEn: String,
  nameAr: String,
  image: String,
  iCost: Number,
  iPrice: Number,
  wCost: Number,
  wPrice: Number,
  dCost: Number,
  dPrice: Number
});
const Cloth = new mongoose.model("Clothe", clothesSchema);

module.exports = Cloth;
