const mongoose = require("mongoose");


//Schema
const userSchema = new mongoose.Schema({
  email: String,
  password: String,
  role: Number,
  firebaseToken: String
});
const User = new mongoose.model("User", userSchema);



module.exports = User;
