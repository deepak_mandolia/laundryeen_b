const mongoose = require("mongoose");
const autoIncrement = require('mongoose-sequence')(mongoose);

const orderSchema = new mongoose.Schema({
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Customer"
  },
  status: Number,
  paymentStatus: Number,
  location: {
    latitude: Number,
    longitude: Number,
    address: String,
    apt: String,
    floor: String,
    bldng: String,
    lockCode: String,
    securityMobile: String
  },
  clothes: [{
    clothID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Clothe"
    },
    nameEn: String,
    nameAr: String,
    iCount: Number,
    iPrice: Number,
    iCost: Number,
    wCount: Number,
    wPrice: Number,
    wCost: Number,
    dCount: Number,
    dPrice: Number,
    dCost: Number,
  }],
  startTime: String,
  endTime: String,
  invoiceTime: String,
  billNo: String,
  minimum: Number,
  subTotal: Number,
  bagFee: Number,
  delFee: Number,
  total: Number
});
orderSchema.plugin(autoIncrement, {
  inc_field: "orderID"
});

const Order = new mongoose.model("Order", orderSchema);

module.exports = Order;
