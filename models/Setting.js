const mongoose = require("mongoose");

//Schema

const coordinateScema = new mongoose.Schema({
  latitude: Number,
  longitude: Number
});

const settingsSchema = new mongoose.Schema({
  type: {
    type: String,
    unique: true
  },
  value: Number,
  coordinates: {
    p1: coordinateScema,
    p2: coordinateScema,
    p3: coordinateScema,
    p4: coordinateScema
  }
});

const Setting = new mongoose.model("Setting", settingsSchema);

module.exports = Setting;
