var request = require('request');
var moment = require('moment-timezone');
const https = require('https');
const querystring = require('querystring');
const fetch = require('node-fetch');
const Firebase = require('../config/firebase');
const User = require('../models/User');
const Customer = require('../models/Customer');

class Helper {

  static async getCurrentTime() {
    return moment().tz("Asia/kuwait").format("YYYY-MM-DD HH:mm:ss");
  }

  static async generateOTP(){
    return Math.floor(1000+Math.random()*9000);
  }

  static async sendSMS(message,phone){
    const appSid = process.env.UNIFONIC_ACCOUNT_SID;

      request({
        method: 'POST',
        url: 'https://api.unifonic.com/rest/Messages/Send',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: "AppSid=" + appSid + "&Recipient=" + phone + "&Body=" + message
      }, function(error, response, body) {
        console.log('Status:', response.statusCode);
        console.log('Headers:', JSON.stringify(response.headers));
        console.log('Response:', body);
      });
    }

    static async sendNotificationToAllUsers(body, title) {
      User.find().then(function(users){
        if (users) {
            var fcm_tokens = [];
            users.forEach(function(user){
              fcm_tokens.push(user.firebaseToken);
            })

              var notification={
                  'title':title,
                  'text':body
              }
              var notification_body = {
                  'notification':notification,
                  'registration_ids':fcm_tokens
              }

              fetch("https://fcm.googleapis.com/fcm/send",{
                  'method':'POST',
                  'headers':{
                      'Authorization':'key='+process.env.FIREBASE_SERVER_KEY
                  },
                  'Content-Type':'application/json',
                   'body':JSON.stringify(notification_body)
              }).then((response)=>{
                  console.log('notificaton send !!')
              }).catch((err)=>{
                  console.log(err)
              })
        }
      })
    }

    static async sendNotificationToCustomer(id, body, title) {
      Customer.findOne({_id:id}).then(function(customer){
        if (customer) {
          var notification={
              'title':title,
              'text':body
          }
          var notification_body = {
              'notification':notification,
              'registration_ids':[customer.firebaseToken]
          }

          fetch("https://fcm.googleapis.com/fcm/send",{
              'method':'POST',
              'headers':{
                  'Authorization':'key='+process.env.FIREBASE_SERVER_KEY
              },
              'Content-Type':'application/json',
               'body':JSON.stringify(notification_body)
          }).then((response)=>{
              console.log('notificaton send !!')
          }).catch((err)=>{
              console.log(err)
          })
        }
      })
    }


    static async createCheckoutID(amount) {
    	const path='/v1/checkouts';
    	const data = querystring.stringify({
    		'entityId':'8a8294174b7ecb28014b9699220015ca',
    		'amount':amount,
    		'currency':'EUR',
    		'paymentType':'DB',
    		'notificationUrl':'http://www.example.com/notify'
    	});
    	const options = {
    		port: 443,
    		host: 'test.oppwa.com',
    		path: path,
    		method: 'POST',
    		headers: {
    			'Content-Type': 'application/x-www-form-urlencoded',
    			'Content-Length': data.length,
    			'Authorization':'Bearer OGE4Mjk0MTc0YjdlY2IyODAxNGI5Njk5MjIwMDE1Y2N8c3k2S0pzVDg='
    		}
    	};
    	return new Promise((resolve, reject) => {
    		const postRequest = https.request(options, function(res) {
    			const buf = [];
    			res.on('data', chunk => {
    				buf.push(Buffer.from(chunk));
    			});
    			res.on('end', () => {
    				const jsonString = Buffer.concat(buf).toString('utf8');
    				try {
    					resolve(JSON.parse(jsonString));
    				} catch (error) {
    					reject(error);
    				}
    			});
    		});
    		postRequest.on('error', reject);
    		postRequest.write(data);
    		postRequest.end();
    	});
    }


    static async checkPaymentStatus(id) {
    	var path='/v1/checkouts/' + id + '/payment';
    	path += '?entityId=8a8294174b7ecb28014b9699220015ca';
    	const options = {
    		port: 443,
    		host: 'test.oppwa.com',
    		path: path,
    		method: 'GET',
    		headers: {
    			'Authorization':'Bearer OGE4Mjk0MTc0YjdlY2IyODAxNGI5Njk5MjIwMDE1Y2N8c3k2S0pzVDg='
    		}
    	};
    	return new Promise((resolve, reject) => {
    		const postRequest = https.request(options, function(res) {
    			const buf = [];
    			res.on('data', chunk => {
    				buf.push(Buffer.from(chunk));
    			});
    			res.on('end', () => {
    				const jsonString = Buffer.concat(buf).toString('utf8');
    				try {
    					resolve(JSON.parse(jsonString));
    				} catch (error) {
    					reject(error);
    				}
    			});
    		});
    		postRequest.on('error', reject);
    		postRequest.end();
    	});
    };

}

module.exports = Helper;
